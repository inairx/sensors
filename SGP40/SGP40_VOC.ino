#include "Adafruit_SGP40.h"
#include "Adafruit_SHT4x.h"

Adafruit_SHT4x Sht4x = Adafruit_SHT4x();
Adafruit_SGP40 Sgp40;
sensors_event_t evHumi, evTmpr;
float    Tmpr;
uint32_t Humi;
uint32_t Tvoc;

void setup() 
{
    Serial.begin(115200);
    while (!Serial) { delay(10); } // Wait for serial console to open!

    Serial.println("\n SGP40 with SHT40 compensation");

    if (!Sgp40.begin())
    {
        Serial.println("SGP40 not found.");
        while (1);
    }

    if (!Sht4x.begin()) // Set to 0x45 for alternate i2c addr
    {  
        Serial.println("SHT40 not found.");
        while (1);
    }

    Sht4x.setPrecision(SHT4X_HIGH_PRECISION);
    Sht4x.setHeater(SHT4X_NO_HEATER);
}

void loop() 
{
    Sht4x.getEvent(&evHumi, &evTmpr);
    Tmpr = evTmpr.temperature;
    Humi = evHumi.relative_humidity;
    if (0 != Tmpr && 0 != Humi)
        Serial.printf("Temperature %3.1f    Humidity %2d \n", Tmpr, Humi);

    Tvoc = Sgp40.measureVocIndex(evTmpr.temperature, evHumi.relative_humidity);
    if (0 < Tvoc)
        Serial.printf("Tvoc Index: %d \n", Tvoc);

    delay(1000);
}
