/*************************************************** 
  This is an example for the SHT4x Humidity & Temp Sensor

  Designed specifically to work with the SHT4x sensor from Adafruit
  ----> https://www.adafruit.com/products/4885

  These sensors use I2C to communicate, 2 pins are required to  
  interface
 ****************************************************/

#include "Adafruit_SHT4x.h"

Adafruit_SHT4x Sht4x = Adafruit_SHT4x();

void setup() 
{
    Serial.begin(115200);

    while (!Serial)
        delay(10);     // will pause Zero, Leonardo, etc until serial console opens

    Serial.println("Adafruit SHT4x test");
    if (! Sht4x.begin()) 
    {
        Serial.println("Couldn't find SHT4x");
        while (1) delay(1);
    }
    Serial.println("Found SHT4x sensor");
    Serial.print("Serial number 0x");
    Serial.println(Sht4x.readSerial(), HEX);

    // You can have 3 different precisions, higher precision takes longer
    Sht4x.setPrecision(SHT4X_HIGH_PRECISION);
    switch (Sht4x.getPrecision()) 
    {
        case SHT4X_HIGH_PRECISION: 
            Serial.println("High precision");
            break;
        case SHT4X_MED_PRECISION: 
            Serial.println("Med precision");
            break;
        case SHT4X_LOW_PRECISION: 
            Serial.println("Low precision");
            break;
    }

    // You can have 6 different heater settings
    // higher heat and longer times uses more power
    // and reads will take longer too!
    Sht4x.setHeater(SHT4X_NO_HEATER);
    switch (Sht4x.getHeater()) 
    {
        case SHT4X_NO_HEATER: 
            Serial.println("No heater");
            break;
        case SHT4X_HIGH_HEATER_1S: 
            Serial.println("High heat for 1 second");
            break;
        case SHT4X_HIGH_HEATER_100MS: 
            Serial.println("High heat for 0.1 second");
            break;
        case SHT4X_MED_HEATER_1S: 
            Serial.println("Medium heat for 1 second");
            break;
        case SHT4X_MED_HEATER_100MS: 
            Serial.println("Medium heat for 0.1 second");
            break;
        case SHT4X_LOW_HEATER_1S: 
            Serial.println("Low heat for 1 second");
            break;
        case SHT4X_LOW_HEATER_100MS: 
            Serial.println("Low heat for 0.1 second");
            break;
    }
}

sensors_event_t Humi, Tmpr;

void loop() 
{
    Sht4x.getEvent(&Humi, &Tmpr); // spent 10ms

    Serial.printf("Temperature: %3.2f C \n", Tmpr.temperature);
    Serial.printf("Humidity:    %3.1f % \n", Humi.relative_humidity);

    delay(1000);
}